Mail
==============

#### Let's Go
general way:
```
npm install && npm start
```
if you want to run this inside a docker container
```
docker build -t gralias/forsaken-mail .
docker run --name forsaken-mail --restart unless-stopped -d -p 25:25 -p 3000:3000 gralias/forsaken-mail
```
Open your browser and type in
```
http://localhost:3000
```

Using `nginx.mail.conf` for nginx config domain 

Enjoy!
